# Лабораторні до MAPZ

Тут знаходяться лабораторні для Інтерпретатора та Паттернів.  
Файлова система папок:

* [Folder - Interpreter](Lab 1 - Interpreter) - Лабораторна по Інтерпретатору. Написана на node.js.
* [Folder - Creational Patterns](Lab 1 - Creational Patterns) - Паттерни: Сінглтон, Фасад.
* [Folder - Structural Patterns](Lab 1 - Structural Patterns) - Паттерни: Прототип, Декоратор, Абстрактна Фабрика.
* [Folder - Behavioral Patterns pt1](Lab 1 - Behavioral Patterns pt1) - Паттерни: Знімок (Memento) + Готовий Проект Lightbot.
* [Folder - Behavioral Patterns pt2](Lab 1 - Behavioral Patterns pt2) - Паттерни: Команда (Command), Спостерігач (Observer).

# LightRobot Interpreter

Інтерпретатор написаний для Лабораторної №1 з Моделювання та Аналізу Програмного Забезпечення.
***
### Getting Started

Інтерпретатор був написаний за допомою JavaScript, а також node.js для дебагу, добавлення модулів і виконання програми в консолі.
Мова - динамічно типізована. Якщо виконувати функції [Print, Move, Turn_, Jump, Light, If_Else] - то для них не потрібно вказувати тип.
Якщо ж це Var, тобто створення змінної - то потрібно вказувати тип - [Var (string) [abc]].
Інтерпретатор не чутливий до регістру. Команди можуть бути як [print], так і [PRINT], і навіть [pRiNt].
Кожна Строга Повинна Закінчуватись символом - [;].

До проекту входять файли :

* [Main.js](Lab 1 - Interpreter/main.js) - Основний Файл. Зберігає в собі Лексер (Tokenizer), Парсер, а також блок основного початку проекту.
* [Slovnyk.js](Lab 1 - Interpreter/slovnyk.js) - Допоміжний файл, у якому прописані типи даних, функції, та їх реалізація для виконання в JavaScript.
* [Program.txt](Lab 1 - Interpreter/Program.txt) - Основний текст програми для виконання.

* [.gitignore](Lab 1 - Interpreter/.gitignore) - Файл ігнорування папки Node_Modules, крім підпапки Colors.
* [Package.json](Lab 1 - Interpreter/package.json) - Файл згенерований через npm init. Являє собою короткі відомості про проект. Версію, виконавчий файл і тд.
* [./Node_Modules](Lab 1 - Interpreter/node_modules) - Папка з Модулями Node.js. До версії 1.2 була в .gitignore, але потім була внесена як вийняток, для загрузки потрібних модулів.
***
### All Function (command's)

1. **Print** - Виводить Значення на Екран.
```
print := 25;                 //Print @ 25
print := 31+38;              //Print @ 69
print := [abc];              //Print @ Значення Змінної abc, при умові, що вона створенна.
print := "Hello World!";     //Print @ Hello World!
```
2. **Move** - Робить 1 Крок Вперед.
```
move := 3;    // Move @ The LightRobot has taken 1 step forward 1/3
              // Move @ The LightRobot has taken 1 step forward 2/3
              // Move @ The LightRobot has taken 1 step forward 3/3
```
3. **Turn_Left** - Виконує 1 Поворот Вліво.
```
turn_left := 5;    // Turn_Left @ The LightRobot made 1 turn to the left 1/5
                   // Turn_Left @ The LightRobot made 1 turn to the left 2/5
                   // Turn_Left @ The LightRobot made 1 turn to the left 3/5
                   // Turn_Left @ The LightRobot made 1 turn to the left 4/5
                      //The LightRobot returned to the same position.
                   // Turn_Left @ The LightRobot made 1 turn to the left 5/5
```
4. **Turn_Right** - Виконує 1 Поворот Вправо.
```
turn_right := 5;    // Turn_Right @ The LightRobot made 1 turn to the left 1/5
                    // Turn_Right @ The LightRobot made 1 turn to the left 2/5
                    // Turn_Right @ The LightRobot made 1 turn to the left 3/5
                    // Turn_Right @ The LightRobot made 1 turn to the left 4/5
                       //The LightRobot returned to the same position.
                    // Turn_Right @ The LightRobot made 1 turn to the left 5/5
```
5. **Jump** - Виконує 1 Прижок Вперед.
```
jump := 1;    //Jump @ The LightRobot just jumped.
```
6. **Light** - Включає/Виключає Світло.
```
light := 2;   //Light @ Light is False. Changing...
              //Light @ Light is True. Changing...
```
7. **Коментарі**
```
// Lorem ipsum dolor sit amet, consectetur adipisicing elit... ;
```
***
8. **Var** - Створює Змінну.
```
var [abc] := 255;   //(Var @ Variable ABC added with Value - 255);
```
9. **If-Else** - ~~Умовний Оператор~~ Умовна Функція.
```
if_else (25>20) := 255;   //Перевіряє Умову в Круглих Дужках. Не виводиться в Дерево, якшо умова = false.
```
