let txt_file = require("fs");
let slovnyk = require("./slovnyk.js").slovnyk
let colors = require("colors");
let http = require("http");
let httpser = require("http-server");
let express = require("express");
let treeify = require("treeify");

class Lexer {
  constructor(text) {
    this.lexemes = [];
    this.i_content = text;
  }

  Tokenizer(text_content, dictionary) {
    let tokens = [];

    let text_without_spaces = text_content.replace(/\s\s+/gm, " ");
    let strok = text_without_spaces.split(";");

    for (var i = 0; i < strok.length; i++) {
      let this_string = strok[i].trim(); //видаляє пробіли

      if(this_string !== "" && this_string[0] != "\/" && this_string[1] != "\/"){
          let parts = this_string.split(":=");
          let funct_part = parts[0].trim();
          let value_part = parts[1].trim();

          let f_funct_part = funct_part.split(" ");

          let funct_of_funct = f_funct_part[0].toLowerCase();
          let condition_of_funct = null;
          let varname_of_funct = null;

          if(/\((.*?)\)/g.test(funct_part)) { //   (condition)
            condition_of_funct = funct_part.match(/\((.*?)\)/g)[0].slice(1, -1);
          }
          if (/\[(.*?)\]/g.test(funct_part)) { //    [name]
              varname_of_funct = funct_part.match(/\[(.*?)\]/g)[0].slice(1, -1);
          }

          let lexObject = {};

          if (varname_of_funct != null || condition_of_funct != null) {
            if (slovnyk["function"][funct_of_funct]){

              if (varname_of_funct != null) {
                if (/[^a-zA-Z0-9]/gm.test(varname_of_funct)) {
                  console.log("Error! Variable consist unacceptable symbols in VarName.");
                  return;
                }
                else {
                  if (/[0-9]/gm.test(varname_of_funct[0])) {
                    console.log("Error! VarName cannot begin with a Number.");
                    return;
                  }
                  else {
                    Object.assign(lexObject, {
                      "function" : {
                        "function" : funct_of_funct,
                        "var_name" : varname_of_funct
                      }
                    });
                  }
                }
              }
              else {
                  if (/[<>]/gm.test(condition_of_funct)) {
                    Object.assign(lexObject, {
                      "function" : {
                        "function" : funct_of_funct,
                        "condition" : condition_of_funct
                      }
                    });
                  }
                  else {
                    console.log("Error! Unknown condition.");
                    return;
                  }
                }
              }
            else {
              Object.assign(lexObject, {
                "function" : {
                  "N/A function" : funct_of_funct
                }
              });
            }
          }
          else {
              if(slovnyk["function"][funct_of_funct]){
                Object.assign(lexObject, {
                  "function" : {
                    "function" : funct_of_funct
                  }
                });
            }
            else {
              Object.assign(lexObject, {
                "function" : {
                  "N/A function" : funct_of_funct
                }
              });
            }
          }
          //Перевірка Оператора Присвоєння
          if(/:=/g.test(this_string)){
            Object.assign(lexObject, {"oper" : ":="});
          }
          else {
            let string_err = "1 Command Without [ := ] , Please Fix It";
            console.log(string_err.black.bgRed);//Немає Оператора Присвоєння""
            console.log("");
            return tokens;
          }

          //Перевірка Типу Значення value
          if (/\"(.*)\"/gim.test(value_part)) {
            if (value_part.length === 3) {
          Object.assign(lexObject, {
            "value" : {
              "type" : "Char",
              "value" : value_part.substring(1,2)
            }
          });
        }
        else {
          let x_lenght = value_part.length - 1;
          Object.assign(lexObject, {
            "value" : {
              "type" : "String",
              "value" : value_part.substring(1,x_lenght)
              }
          });
        }
      }
          else {
            if (/\[(.*)\]/gim.test(value_part)) {
              let var_lenght = value_part.length - 1;
              Object.assign(lexObject, {
                "value" : {
                  "type" : "Variable",
                  "value" : value_part.substring(1,var_lenght)
                }
              });
            }
            else {
              if(Number(value_part)) {
                if(Number(value_part) % 1 == 0){
                  if (Number(value_part) > -1000000 && Number(value_part) < 1000000) {
                    Object.assign(lexObject, {
                      "value" : {
                        "type" : "Integer",
                        "value" : Number(value_part)
                      }
                    });
                  }
                  else {
                    Object.assign(lexObject, {
                      "value" : {
                        "type" : "Long-Integer",
                        "value" : Number(value_part)
                      }
                    });
                  }
                }
                else {
                  if (Number(value_part) < 1000000 && Number(value_part) > -1000000) {
                    Object.assign(lexObject, {
                      "value" : {
                        "type" : "Float",
                        "value" : Number(value_part)
                      }
                    });
                  }
                  else {
                    Object.assign(lexObject, {
                      "value" : {
                        "type" : "Double",
                        "value" : Number(value_part)
                      }
                    });
                  }
                }
              }
              else {
                  Object.assign(lexObject, {
                    "value" : {
                      "type" : "N/A Value",
                      "value" : value_part
                    }
                  });
                }
              }

            }

          tokens.push(lexObject);

        }
      }

    return tokens;
  }
}

class Parser {
  constructor(text) {
    this.lexemes = [];
    this.i_content = text;
  }

  // Funct_Parser(tokens, dictionary) {
  //       if (tokens[i]["function"] && dictionary["function"][functionName.toLowerCase()]) {
  //         let this_funct = dictionary["function"][functionName.toLowerCase()];
  //
  //         this_funct(valueName);
  //       }
  //       else {
  //         console.log(`Error! Function [ ${functionName.toUpperCase()} ] isn't Registered!`);
  //       }
  //
  // }

  Funct_Parser(tokens, dictionary) {
    let arr_tree = [];
    let array_variables = [];
    let var_count = 0;

    if(tokens && tokens.length != 0){

        for (var i = 0; i < tokens.length; i++) {

          let treeobj = {};
          let var_object = {};
          let found = false;

            let par_function = null;
                if(par_function = tokens[i]["function"]["N/A function"]){
                  par_function = tokens[i]["function"]["N/A function"];
                } //Добавити, Якщо Функція == N/A - не Виводити Її в Граф
                else {
                  par_function = tokens[i]["function"]["function"];
                }

            let par_condition = null;
            let par_name = null;

            let par_value_type = tokens[i]["value"]["type"];
            let par_value = tokens[i]["value"]["value"];

            if(tokens[i]["function"]["function"] == "var"){
              par_name = tokens[i]["function"]["var_name"];
            }
            if(tokens[i]["function"]["function"] == "if_else"){
              par_condition = tokens[i]["function"]["condition"];
            }

            if(par_function == "print"){
              if (par_value_type == "Variable"){

                for (var j = 0; j < array_variables.length; j++) {

                  if (array_variables[j].name == par_value) {
                    let this_funct = slovnyk["function"][par_function];
                    this_funct(par_value_type, par_value);
                    found = true;
                  }
                }
                if (found == false) {
                  console.log("Error! There is no such variable");
                }

              }
              else {
                let this_funct = slovnyk["function"][par_function];
                this_funct(par_value_type, par_value);
              }
            }
            else if (par_function == "move" || par_function == "turn_left" || par_function == "turn_right" || par_function == "jump") {
              let this_funct = slovnyk["function"][par_function];
              this_funct(par_value);
            }
            else if (par_function == "light") {
              let this_funct = slovnyk["function"][par_function];
              this_funct(par_value);
            }
            else if (par_function == "var") {

              let copy_par_value = eval(par_value);


              Object.assign(var_object, {
                  "name" : par_name,
                  "value" : copy_par_value
              });

              console.log(`Var @ Variable `+`${par_name.toUpperCase()}`.bold+ ` added with Value - `+`${par_value}`.bold);
              var_count++;
              array_variables.push(var_object);
              // console.log(array_variables);
              // console.log(var_count);

            }
            else if (par_function == "if_else") {
              // console.log("Вислів - " + par_condition);
              // console.log("Тип Значення - " + par_value_type);
              // console.log("Значення - " + par_value);

              let this_funct = slovnyk["function"][par_function];
              this_funct(par_condition, par_value_type, par_value);

            }
            else {
              console.log(`Error! Function `.black.bgBrightRed,`[${par_function.toUpperCase()}]`.zebra,` isn't Registered!`.black.bgBrightRed);
              console.log("");
            }

            if (par_function == "light" || par_function == "move" || par_function == "turn_left" || par_function == "turn_right" || par_function == "jump") {
              treeobj = treeify.asTree({
                function: par_function.bold,
                st_ex : {
                  operator: ":=".bold,
                  st_ex : {
                    value : `${par_value}`.bold
                  }
                }
              }, true);
            }
            else  if (par_function == "print") {

              // console.log("111111 - " + par_value_type);

              let pr_copy_par_value = null;

              if (par_value_type == "String" || par_value_type == "Char") {
                pr_copy_par_value = par_value;
              } else {
                pr_copy_par_value = eval(par_value);
              }

                treeobj = treeify.asTree({
                  function: par_function.bold,
                  st_ex : {
                    operator: ":=".bold,
                    st_ex : {
                      value : `${pr_copy_par_value}`.bold
                    }
                  }
                }, true);

              }
             else if (par_function == "var") {
                treeobj = treeify.asTree({
                  function: par_function.bold,
                  st_ex : {
                    name_of : par_name.bold,
                    st_ex : {
                      operator: ":=".bold,
                      st_ex : {
                        value : `${par_value}`.bold
                      }
                    }
                  }
                }, true);
            }
            else  if (par_function == "if_else") {
              let copy_par_condition = eval(par_condition);

              if (copy_par_condition == true) {

                if (/</g.test(par_condition)) {
                  let chastina = par_condition.split("<");
                  let i_first_number = chastina[0];
                  let i_second_number = chastina[1];

                  console.log(
                    treeify.asTree({
                      function: par_function.bold,
                      st_ex : {
                        first_number : i_first_number.bold,
                        st_ex : {
                          condition : "<".bold,
                          st_ex :{
                            second_number : i_second_number.bold,
                            st_ex : {
                              operator: ":=".bold,
                              st_ex : {
                                value : `${par_value}`.bold
                              }
                            }
                          }
                        }
                      }
                    }, true)
                  );
                } else  if (/>/g.test(par_condition)) {
                  let chastina = par_condition.split(">");
                  let i_first_number = chastina[0];
                  let i_second_number = chastina[1];

                  treeobj = treeify.asTree({
                      function: par_function.bold,
                      st_ex : {
                        first_number : i_first_number.bold,
                        st_ex : {
                          condition : ">".bold,
                          st_ex :{
                            second_number : i_second_number.bold,
                            st_ex : {
                              operator: ":=".bold,
                              st_ex : {
                                value : `${par_value}`.bold
                              }
                            }
                          }
                        }
                      }
                    }, true);
                } else {
                  console.log("Error! Incorrect Condition");
                }

              } else {
                console.log("Condition from If-Else = False. TreeView will not logg to consol.".black.bgBrightRed);
              }
            } else {
              console.log("Error! N/A Function");
            }

            // console.log("Fuctnion - " + par_function);
            // console.log("Condition - " + par_condition);
            // console.log("Name For Variable - " + par_name);
            // console.log("Value Type - " + par_value_type);
            // console.log("Value - " + par_value);
            // console.log(`F:${par_function} T:${par_condition} N:${par_name} VT:${par_value_type} V:${par_value}`);

            arr_tree.push(treeobj);
        }
    }
    else {
      console.log("Error! Tokens for Parser is Empty!".black.bgBrightRed);
      console.log("");
    }

    return arr_tree;
  }
}

  txt_file.readFile("./program.txt", "utf-8", function(error, text_content){

    if (error == null) {
      var lexer = new Lexer(text_content);

      var final_tokens = lexer.Tokenizer(text_content, slovnyk);
      console.log(JSON.stringify(final_tokens, null, 4));
      console.log("");


      console.log("\n#### BEGIN ####\n".brightGreen);
      //
      var parser = new Parser(text_content);
      let treee = parser.Funct_Parser(final_tokens, slovnyk);

      console.log("\n##### END #####\n".brightRed);


      // let tree_function = "Print";
      // let tree_oper = ":=";
      // let tree_value = "2+2";


      // let tratata = treeify.asTree({
      //     function: tree_function,
      //     st_ex : {
      //       operator: tree_oper,
      //       st_ex : {
      //         value : tree_value
      //       }
      //     }
      //   }, true);

        console.log("Tree of Statement's\n" + treee);


    }
    else {
      console.log(`Error! #${error}`.black.bgBrightRed);
      console.log("");
    }
  });
