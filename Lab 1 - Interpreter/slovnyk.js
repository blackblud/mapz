let slovnyk = {
  "function" : {
    print : function (type_element, element) {


      if(type_element == "String" || type_element == "Char" || type_element == "Variable"){
        console.log("Print @ " + element.underline);
      } else {
        let opera_element = eval(element);
        if (opera_element != "Infinity") {

          console.log(`Print @ `,`${opera_element}`.underline);
        } else {
          console.log("Print @ Не Можна Ділити на Нуль!");
        }
      }
          console.log("");
    },
    move : function (el) {
      let step = 1;
      for (var i = 0; i < el; i++) {
        console.log(`Move @ The LightRobot has taken 1 step forward  ${step++}/${el}`.black.bgCyan);
      }
      console.log("");
    },
    turn_left : function (el) {
      let step = 1;
      for (var i = 0; i < el; i++) {
        console.log(`Turn_Left @ The LightRobot made 1 turn to the left  ${step++}/${el}`.black.bgYellow);
        if(step%4 == 1){

          console.log(`The LightRobot returned to the same position.`.red.bgYellow);
          console.log("");
        }
      }
      console.log("");
    },
    turn_right : function (el) {
      let step = 1;
      for (var i = 0; i < el; i++) {
        console.log(`Turn_Right @ The LightRobot made 1 turn to the right  ${step++}/${el}`.black.bgYellow);
        if(step%4 == 1){

          console.log(`The LightRobot returned to the same position.`.red.bgYellow);
          console.log("");
        }
      }
      console.log("");
    },
    jump : function (el) {
      let step = 1;
      for (var i = 0; i < el; i++) {

        console.log(`Jump @ The LightRobot just jumped.  ${step++}/${el}`.black.bgGreen);
      }
      console.log("");
    },
    light : function (el) {
      let i_light = false;
      for (var i = 0; i < el; i++) {


        if (i_light == false) {
          console.log(`Light @ Light is ${i_light}.`.black.bgBrightRed,`Changing...`.black.bgBrightGreen);
        } else {
          console.log(`Light @ Light is ${i_light}. `.black.bgBrightGreen, `Changing...`.black.bgBrightRed);
        }



        if (i_light == false) {
          i_light = true;
        } else {
          i_light = false;
        }
      }
      console.log("");
    },
    var : function (el) {

      // let copy_par_value = eval(par_value);
      //
      //
      // Object.assign(var_object, {
      //     "name" : par_name,
      //     "value" : copy_par_value
      // });
      //
      // console.log(`Var @ Variable `+`${par_name}`.bold+ ` added with Value - `+`${par_value}`.bold);
      // var_count++;
      // array_variables.push(var_object);
      // console.log(array_variables);
      // console.log(var_count);
    },
    if_else : function (cond, el_type, el) {
      let condition_out = eval(cond);
      if (condition_out == true) {
        slovnyk["function"]["print"](el_type ,el);
      } else {
        // console.log("falseeeee");
      }
    }
  },
  "type" : [
    "int",
    "longint",
    "float",
    "double",
    "string",
    "char",
    "variable"
  ]
};

module.exports.slovnyk = slovnyk;
