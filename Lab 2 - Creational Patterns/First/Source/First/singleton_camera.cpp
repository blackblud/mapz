// Fill out your copyright notice in the Description page of Project Settings.


#include "singleton_camera.h"

// Sets default values
Asingleton_camera::Asingleton_camera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Singl_camera_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MyCameraMesh"));
	Singl_camera_scene = CreateDefaultSubobject<USceneComponent>(TEXT("MyScene"));

	RootComponent = Singl_camera_scene;
	Singl_camera_mesh->SetupAttachment(RootComponent);

	//root - ����� �� AActor, �����������.
}

// Called when the game starts or when spawned
void Asingleton_camera::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Asingleton_camera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Singl_camera_scene->AddLocalRotation(FRotator(0.f, 0.f, 1.f), false);

}

