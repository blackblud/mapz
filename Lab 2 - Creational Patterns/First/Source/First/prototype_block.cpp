// Fill out your copyright notice in the Description page of Project Settings.


#include "prototype_block.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
Aprototype_block::Aprototype_block()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>("iBlockMesh");

	SetRootComponent(BlockMesh);

	BlockMovement = CreateDefaultSubobject<UProjectileMovementComponent>("iBlockMovement");
	BlockMovement->InitialSpeed = 2000.f;
	BlockMovement->MaxSpeed = 2000.f;

}

// Called when the game starts or when spawned
void Aprototype_block::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void Aprototype_block::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

