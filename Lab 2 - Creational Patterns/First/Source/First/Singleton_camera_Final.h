// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Singleton_camera_Final.generated.h"

UCLASS()
class FIRST_API ASingleton_camera_Final : public APawn
{
	GENERATED_BODY()

private:
	// Sets default values for this pawn's properties
	ASingleton_camera_Final();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Amount);
	void MoveRight(float Amount);

	void Turn(float Amount);
	void LookUp(float Amount);

	//void PlaceProto();

	class UFloatingPawnMovement* FloatingPawnMovement;

	UPROPERTY(EditAnywhere, Category = "Patterns")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, Category = "Patterns")
	class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Patterns")
	class USpringArmComponent* CameraArm;


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//UPROPERTY(EditAnywhere, Category = "Patterns")
	//ASingleton_camera_Final* getInstance();

};
