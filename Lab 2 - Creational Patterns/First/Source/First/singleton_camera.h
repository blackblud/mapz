// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "singleton_camera.generated.h"

UCLASS()
class FIRST_API Asingleton_camera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Asingleton_camera();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Camera Components")
	UStaticMeshComponent* Singl_camera_mesh;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite, Category = "Camera Components")
	USceneComponent* Singl_camera_scene;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Booleanes")
	bool MyBool;
};
