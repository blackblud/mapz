// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "prototype_block.generated.h"

UCLASS()
class FIRST_API Aprototype_block : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Aprototype_block();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "iPatterns")
	UStaticMeshComponent* BlockMesh;

	UPROPERTY(EditAnywhere, Category = "iPatterns")
	class UProjectileMovementComponent* BlockMovement;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
