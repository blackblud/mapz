// Copyright Epic Games, Inc. All Rights Reserved.

#include "FirstGameMode.h"
#include "FirstPlayerController.h"
#include "FirstCharacter.h"
#include "UObject/ConstructorHelpers.h"

#include "MenuHUD.h"
#include "MenuPlayerController.h"

AFirstGameMode::AFirstGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AFirstPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	PlayerControllerClass = AMenuPlayerController::StaticClass();
	HUDClass = AMenuHUD::StaticClass();
}