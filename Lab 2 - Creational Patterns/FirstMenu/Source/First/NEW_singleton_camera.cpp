// Fill out your copyright notice in the Description page of Project Settings.


#include "NEW_singleton_camera.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ANEW_singleton_camera::ANEW_singleton_camera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("iStaticMeshComponent");



}

// Called when the game starts or when spawned
void ANEW_singleton_camera::BeginPlay()
{
	Super::BeginPlay();

	StaticMesh->SetWorldScale3D(FMath::VRand());
	
}

// Called every frame
void ANEW_singleton_camera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

