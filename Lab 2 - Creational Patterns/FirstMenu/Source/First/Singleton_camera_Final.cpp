// Fill out your copyright notice in the Description page of Project Settings.


#include "Singleton_camera_Final.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
ASingleton_camera_Final::ASingleton_camera_Final()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FloatingPawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>("iPawnMovement");

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("iStaticMeshComponent");

	CameraArm = CreateDefaultSubobject<USpringArmComponent>("iCameraArmComponent");
	CameraArm->SetupAttachment(StaticMesh);
	CameraArm->TargetArmLength = 500.f;

	Camera = CreateDefaultSubobject<UCameraComponent>("iCameraComponent");
	Camera->SetRelativeLocation(FVector(-400.f, 200.f, 0.f));
	Camera->SetRelativeRotation(FRotator(0.f, 325.f, 0.f));
	Camera->SetupAttachment(CameraArm);

	SetRootComponent(StaticMesh);

	bUseControllerRotationYaw = true;
	bUseControllerRotationPitch = true;
}

// Called when the game starts or when spawned
void ASingleton_camera_Final::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASingleton_camera_Final::MoveForward(float Amount)
{
	FloatingPawnMovement->AddInputVector(GetActorForwardVector() * Amount);
}

void ASingleton_camera_Final::MoveRight(float Amount)
{
	FloatingPawnMovement->AddInputVector(GetActorRightVector() * Amount);
}

void ASingleton_camera_Final::Turn(float Amount)
{
	AddControllerYawInput(Amount);
}

void ASingleton_camera_Final::LookUp(float Amount)
{
	AddControllerPitchInput(Amount);
}

// Called every frame
void ASingleton_camera_Final::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASingleton_camera_Final::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &ASingleton_camera_Final::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASingleton_camera_Final::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &ASingleton_camera_Final::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &ASingleton_camera_Final::LookUp);

}

