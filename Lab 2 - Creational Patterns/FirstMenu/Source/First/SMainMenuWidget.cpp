// Fill out your copyright notice in the Description page of Project Settings.


#include "SMainMenuWidget.h"
#include "MenuHUD.h"
#include "GameFramework/PlayerController.h"

#define LOCTEXT_NAMESPACE "MainMenu"

void SMainMenuWidget::Construct(const FArguments& InArgs)
{

	bCanSupportFocus = true;
	OwningHUD = InArgs._OwningHUD;

	const FMargin ContentPadding = FMargin(500.f, 300.f);
	const FMargin ButtonPadding = FMargin(10.f);

	const FText TitleText = LOCTEXT("Lightbot", "Game for MAPZ");

	const FText PlayText = LOCTEXT("PlayGame", "Play Game");
	const FText LevelText = LOCTEXT("Levels", "Levels");
	const FText SettingsText = LOCTEXT("Settings", "Settings");
	const FText ExitText = LOCTEXT("ExitGame", "Exit Game");

	FSlateFontInfo ButtonTextStyle = FCoreStyle::Get().GetFontStyle("EmbossedText");
	ButtonTextStyle.Size = 40.f;

	FSlateFontInfo TitleTextStyle = ButtonTextStyle;
	TitleTextStyle.Size = 60.f;


	ChildSlot
		[
			SNew(SOverlay)
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			[
				SNew(SImage)
				.ColorAndOpacity(FColor::Red)
			]
			+ SOverlay::Slot()
			.HAlign(HAlign_Fill)
			.VAlign(VAlign_Fill)
			.Padding(ContentPadding)
			[
				SNew(SVerticalBox)
				+ SVerticalBox::Slot()
				[
					SNew(STextBlock)
					.ColorAndOpacity(FColor::Black)
					.Font(TitleTextStyle)
					.Text(TitleText)
					.Justification(ETextJustify::Center)
				]

				//Play
				+ SVerticalBox::Slot()
				.Padding(ButtonPadding)
				[
					SNew(SButton)
					.OnClicked(this, &SMainMenuWidget::OnPlayClicked)
					[
						SNew(STextBlock)
						.Font(ButtonTextStyle)
						.Text(PlayText)
						.Justification(ETextJustify::Center)
					]
				]

				//Level
				+ SVerticalBox::Slot()
					.Padding(ButtonPadding)
					[
						SNew(SButton)
						.OnClicked(this, &SMainMenuWidget::OnLevelClicked)
						.IsEnabled(false)
					[
						SNew(STextBlock)
						.Font(ButtonTextStyle)
						.Text(LevelText)
						.Justification(ETextJustify::Center)
					]
				]

				//Setting
				+ SVerticalBox::Slot()
				.Padding(ButtonPadding)
				[
					SNew(SButton)
					.OnClicked(this, &SMainMenuWidget::OnSettingsClicked)
					[
						SNew(STextBlock)
						.Font(ButtonTextStyle)
						.Text(SettingsText)
						.Justification(ETextJustify::Center)
					]
				]

				//Exit
				+ SVerticalBox::Slot()
				.Padding(ButtonPadding)
				[
					SNew(SButton)
					.OnClicked(this, &SMainMenuWidget::OnExitClicked)
					[
						SNew(STextBlock)
						.Font(ButtonTextStyle)
						.Text(ExitText)
						.Justification(ETextJustify::Center)
					]
				]



			]
		];




}

FReply SMainMenuWidget::OnPlayClicked() const
{
	if (OwningHUD.IsValid()) {
		OwningHUD->CloseMenu();
	}

	return FReply::Handled();
}

FReply SMainMenuWidget::OnLevelClicked() const
{
	return FReply::Handled();
}

FReply SMainMenuWidget::OnSettingsClicked() const
{
	return FReply::Handled();
}

FReply SMainMenuWidget::OnExitClicked() const
{
	if (OwningHUD.IsValid()) {
		if (APlayerController* PC = OwningHUD->PlayerOwner) {
			PC->ConsoleCommand("quit");
		}
	}

	return FReply::Handled();
}

#undef LOCTEXT_NAMESPACE