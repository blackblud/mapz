// Fill out your copyright notice in the Description page of Project Settings.


#include "prototype_clone.h"
#include "prototype_block.h"

#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values
Aprototype_clone::Aprototype_clone()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FloatingPawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>("iPawnMovement");

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("iStaticMeshComponent");

	CameraArm = CreateDefaultSubobject<USpringArmComponent>("iCameraArmComponent");
	CameraArm->SetupAttachment(StaticMesh);
	CameraArm->TargetArmLength = 500.f;

	Camera = CreateDefaultSubobject<UCameraComponent>("iCameraComponent");
	Camera->SetRelativeLocation(FVector(-400.f, 0.f, 150.f));
	Camera->SetupAttachment(CameraArm);

	SetRootComponent(StaticMesh);

	bUseControllerRotationYaw = true;
	bUseControllerRotationPitch = true;
}

// Called when the game starts or when spawned
void Aprototype_clone::BeginPlay()
{
	Super::BeginPlay();
	
}

void Aprototype_clone::MoveForward(float Amount)
{
	FloatingPawnMovement->AddInputVector(GetActorForwardVector() * Amount);
}

void Aprototype_clone::MoveRight(float Amount)
{
	FloatingPawnMovement->AddInputVector(GetActorRightVector() * Amount);
}

void Aprototype_clone::Turn(float Amount)
{
	AddControllerYawInput(Amount);
}

void Aprototype_clone::LookUp(float Amount)
{
	AddControllerPitchInput(Amount);
}

void Aprototype_clone::Clone()
{
	if (BlockClass) {
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		SpawnParams.bNoFail = true;
		SpawnParams.Owner = this;
		SpawnParams.Instigator = this;

		FTransform BlockSpawnTransform;
		BlockSpawnTransform.SetLocation(GetActorForwardVector() * 500.f + GetActorLocation());
		BlockSpawnTransform.SetRotation(GetActorRotation().Quaternion());
		BlockSpawnTransform.SetScale3D(FVector(1.f));

		GetWorld()->SpawnActor<Aprototype_block>(BlockClass, BlockSpawnTransform, SpawnParams);
	}
}

// Called every frame
void Aprototype_clone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void Aprototype_clone::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Clone()", IE_Pressed, this, &Aprototype_clone::Clone);

	PlayerInputComponent->BindAxis("MoveForward", this, &Aprototype_clone::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &Aprototype_clone::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &Aprototype_clone::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &Aprototype_clone::LookUp);
}

