// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "prototype_clone.generated.h"

UCLASS()
class SECOND_API Aprototype_clone : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	Aprototype_clone();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Amount);
	void MoveRight(float Amount);

	void Turn(float Amount);
	void LookUp(float Amount);

	void Clone();

	UPROPERTY(EditAnywhere, Category = "Patterns")
		TSubclassOf<class Aprototype_block> BlockClass;

	class UFloatingPawnMovement* FloatingPawnMovement;

	UPROPERTY(EditAnywhere, Category = "Patterns")
		UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, Category = "Patterns")
		class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, Category = "Patterns")
		class USpringArmComponent* CameraArm;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
