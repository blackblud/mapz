// Fill out your copyright notice in the Description page of Project Settings.


#include "FactoryLevel.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFactoryLevel::AFactoryLevel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFactoryLevel::BeginPlay()
{
	Super::BeginPlay();
	
	//UGameplayStatics::OpenLevel(this, "Level2");
}

// Called every frame
void AFactoryLevel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

